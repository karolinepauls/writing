Welcome
=======

I'm a (mostly) Python developer and DevOps engineer, though I've also got frontend development and
full stack on my record.

I write about thoughts and challenges I deem worth sharing. In my free time I'm currently focused on
my N-th attempt to learn guitar.

In order to **contact me**, mail me at ``contact@<domain name from the address bar>``.
Alternatively, ``base64 -d <<<"Y29udGFjdEBrYXJvbGluZXBhdWxzLmNvbQo="``.


Table of contents
-----------------

.. toctree::
   :maxdepth: 2

   articles/exceptions.rst
   articles/why-your-team-will-procrastinate-reviewing-your-pr.rst
   articles/dbrewind.rst
   articles/python-porting-pickles.rst
   articles/pdbpp.rst
   articles/set-up-your-test-database-from-migrations.rst


.. figure:: https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-nc-sa.svg
  :alt: Attribution-NonCommercial-ShareAlike 4.0 International

  All content of this website is licenced under the `Creative Commons
  Attribution-NonCommercial-ShareAlike 4.0 International
  <https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode>`_ licence.
